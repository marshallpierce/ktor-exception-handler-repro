package org.mpierce.ktor

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.StatusPages
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import kotlinx.coroutines.async
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class KtorExceptionHandlingCoroutineTest {
    val appInit: Application.() -> Unit = {
        install(StatusPages) {
            exception<KaboomException> {
                call.respond(HttpStatusCode.Conflict, "kaboom")
            }
        }
        install(Routing)

        routing {
            get("/exception-in-handler") {
                throw KaboomException()
            }
            get("/exception-in-async") {
                async {
                    throw KaboomException()
                }.await()
            }
        }

    }

    @Test
    internal fun exceptionFromRouteHandlerWorks() {
        withTestApplication(appInit) {
            with(handleRequest(HttpMethod.Get, "/exception-in-handler")) {
                assertEquals(HttpStatusCode.Conflict, response.status())
            }
        }
    }

    @Test
    internal fun exceptionFromRouteHandlerInAsyncBlockWorks() {
        withTestApplication(appInit) {
            with(handleRequest(HttpMethod.Get, "/exception-in-async")) {
                assertEquals(HttpStatusCode.Conflict, response.status())
            }
        }
    }
}

class KaboomException : Exception()
